/*
** EPITECH PROJECT, 2018
** tetris
** File description:
** Get the shape of the tetrimino
*/

#include <stdlib.h>
#include "tetris.h"
#include "get_next_line.h"

static int	strlen_shape(char *line)
{
	int i = 0;
	int len = 0;

	while (line[i] != '\0') {
		if (line[i] == '*')
			len = i + 1;
		i++;
	}
	return (len);
}

static bool	check_line(tetrimino_t *tetrimino, int index)
{
	int len = strlen_shape(tetrimino->shape[index]);

	if (len > tetrimino->width)
		return (false);
	tetrimino->shape[index][len] = '\0';
	return (true);
}

static void	change_line(char **line, int fd)
{
	free(*line);
	*line = get_next_line(fd);
}

int	get_shape(tetrimino_t *tetrimino, int fd)
{
	char *line = get_next_line(fd);

	while (line && strlen_shape(line) == 0)
		change_line(&line, fd);
	for (int i = 0; i < tetrimino->height; i++) {
		tetrimino->shape[i] = line;
		if (tetrimino->shape[i] == NULL || !check_line(tetrimino, i))
			return (84);
		line = get_next_line(fd);
	}
	tetrimino->shape[tetrimino->height] = NULL;
	line = get_next_line(fd);
	while (line) {
		if (strlen_shape(line) != 0)
			return (84);
		free(line);
		line = get_next_line(fd);
	}
	return (0);
}
