/*
** EPITECH PROJECT, 2018
** Tetris
** File description:
** main.c
*/

#include <curses.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include "my.h"
#include "tetris.h"

void	display_help(char *name)
{
	write(1, "Usage: ", 7);
	write(1, name, my_strlen(name));
	write(1, " [options]\nOptions:\n  --help\t\t", 30);
	write(1, "Display this help\n  -L --level={num}\t", 37);
	write(1, "Start Tetris at level num (def: 1)\n  -l --key-left={K}",54);
	write(1, "\tMove the tetrimino LEFT using the K key ", 41);
	write(1, "(def: left arrow)\n  -r --key-right={K}\t", 39);
	write(1, "Move the tetrimino RIGHT using the K key", 40);
	write(1, "(def: right arrow)\n  -t --key-turn={K}\t", 39);
	write(1, "TURN the tetrimino clockwise 90d using the K key ", 49);
	write(1, "(def: top arrow)\n  -d --key-drop={K}\t", 37);
	write(1, "DROP the tetrimino using the K key (def: down arrow)", 52);
	write(1, "\n  -q --key-quit={K}\tQUIT the game using the K key ", 51);
	write(1, "(def: \'q\' key)\n  -p --key-pause={K}\t", 36);
	write(1, "PAUSE/RESTART the game using the K key ", 39);
	write(1, "(def: space bar)\n  --map-size={row,col}\t", 40);
	write(1, "Set the numbers of rows and columns of the map ", 47);
	write(1, "(def: 20,10)\n  -w --without-next\tHide next tetrimino ", 53);
	write(1, "(def: false)\n  -D --debug\t\tDebug mode (def: false)\n", 51);
}

/*
** option[0]: 0-> no next, 1->next
** option[1]: begining level
** option[2]: nb row
** option[3]: nb col
**
** key[0]: left key
** key[1]: right key
** key[2]: turn key
** key[3]: drop key
** key[4]: quit key
** key[5]: pause key
*/
int	main(int argc, char **argv)
{
	int option[4] = {1, 1, 20, 10};
	char key[6][6] = {"^[[D", "^[[C", "^[[A", "^[[B", "q", " "};
	int ret = 0;
	tetrimino_t **piece = NULL;

	ret = get_opt(argc, argv, &option[0], &key[0]);
	if (ret == 84) {
		return (84);
	} else if (ret == -1)
		return (0);
	if (ret == 1) {
		ret = start_debug_mode(key, option);
	} else {
		piece = get_tetriminos(-1);
		ret = tetris(key, option, piece);
	}
	return (ret);
}
