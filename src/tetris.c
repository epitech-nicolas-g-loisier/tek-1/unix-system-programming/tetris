/*
** EPITECH PROJECT, 2018
** tetris
** File description:
** Main game function
*/

#include <stdlib.h>
#include <unistd.h>
#include <curses.h>
#include "tetris.h"
#include "my.h"

int	print_game(char **frame)
{
	int idx = 0;

	while (frame[idx] != NULL) {
		mvprintw(idx, 33, "%s", frame[idx]);
		idx++;
	}
	return (0);
}

int	is_key_pressed(void)
{
	int status = getch();

	if (status == -1)
		return (0);
	ungetch(status);
	return (1);
}

void	get_key(char *buffer)
{
	int i = 0;
	int status = getch();

	while (status != -1) {
		buffer[i] = status;
		i++;
		status = getch();
	}
	buffer[i] = '\0';
}

int	init_window(int *size)
{
	initscr();
	if (COLS <= size[1] * 2 + 34 || LINES <= size[0] + 2) {
		endwin();
		return (84);
	}
	curs_set(0);
	timeout(0);
	cbreak();
	noecho();
	start_color();
	init_pair(1, COLOR_RED, COLOR_BLACK);
	init_pair(2, COLOR_GREEN, COLOR_BLACK);
	init_pair(3, COLOR_YELLOW, COLOR_BLACK);
	init_pair(4, COLOR_BLUE, COLOR_BLACK);
	init_pair(5, COLOR_MAGENTA, COLOR_BLACK);
	init_pair(6, COLOR_CYAN, COLOR_BLACK);
	init_pair(7, COLOR_WHITE, COLOR_BLACK);
	attron(A_BOLD);
	return (0);
}

int	tetris(char controls[6][6], int options[4], tetrimino_t **pieces)
{
	char buffer[6] = "\0";
	char *map = create_map(&options[2]);
	char **frame = get_game_frame(options[2], options[3]);
	int score = 0;
	int line = 0;
	int level = 0;

	if (frame == NULL || map == NULL || init_window(&options[2]) == 84)
		return (84);
	while (my_strcmp(controls[4], buffer) != 0) {
		print_game(frame);
		print_data(map, &options[2]);
		tetris_info(score, line, level);
		refresh();
		if (is_key_pressed())
			get_key(&buffer[0]);
	}
	end_win(pieces, map, frame);
	return (0);
}
