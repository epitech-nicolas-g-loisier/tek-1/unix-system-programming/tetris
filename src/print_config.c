/*
** EPITECH PROJECT, 2018
** tetris
** File description:
** Prints the game config
*/

#include "my.h"

static void	print_key(char *key)
{
	if (my_strcmp(key, " ") == 0)
		my_putstr("(space)");
	else if (my_strncmp(key, "^[[", 3) == 0) {
		my_putstr("^EO");
		my_putstr(&key[3]);
	} else
		my_putstr(key);
}

static void	print_next_option(int opt)
{
	if (opt)
		my_putstr("\nNext :  Yes\nLevel :  ");
	else
		my_putstr("\nNext :  No\nLevel :  ");
}

void	print_config(char controls[6][6], int *options)
{
	my_putstr("*** DEBUG MODE ***\nKey Left :  ");
	print_key(controls[0]);
	my_putstr("\nKey Right :  ");
	print_key(controls[1]);
	my_putstr("\nKey Turn :  ");
	print_key(controls[2]);
	my_putstr("\nKey Drop :  ");
	print_key(controls[3]);
	my_putstr("\nKey Quit :  ");
	print_key(controls[4]);
	my_putstr("\nKey Pause :  ");
	print_key(controls[5]);
	print_next_option(options[0]);
	my_put_nbr(options[1]);
	my_putstr("\nSize :  ");
	my_put_nbr(options[2]);
	my_putchar('*');
	my_put_nbr(options[3]);
}
