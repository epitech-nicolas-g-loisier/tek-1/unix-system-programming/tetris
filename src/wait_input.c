/*
** EPITECH PROJECT, 2018
** tetris
** File description:
** Loops while no input is given
*/

#include <curses.h>
#include <unistd.h>

void	wait_input(void)
{
	char buffer[6] = "";
	int status = 0;

	initscr();
	noecho();
	while (status == 0)
		status = read(0, buffer, 5);
	endwin();
}
