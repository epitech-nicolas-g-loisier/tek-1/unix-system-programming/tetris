/*
** EPITECH PROJECT, 2018
** tetris
** File description:
** Creates a map to be used in the game
*/

#include <stdlib.h>

char	*create_map(int *size)
{
	char *map = malloc(sizeof(char) * (size[0] * size[1] + 1));

	if (map == NULL)
		return (NULL);
	for (int i = 0; i < size[0] * size[1]; i++)
		map[i] = ' ';
	map[size[0] * size[1]] = '\0';
	return (map);
}
