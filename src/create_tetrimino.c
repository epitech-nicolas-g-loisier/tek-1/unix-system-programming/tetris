/*
** EPITECH PROJECT, 2018
** tetris
** File description:
** Creates a tetrimino from file
*/

#include <limits.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include "get_next_line.h"
#include "tetris.h"
#include "my.h"

static int	clear_buffer(int fd)
{
	char *line = get_next_line(fd);

	while (line) {
		free(line);
		line = get_next_line(fd);
	}
	return (84);
}

static int	parse_data(tetrimino_t *tetrimino, char *line, int depth)
{
	int i = 0;

	while (line[i] >= '0' && line[i] <= '9')
		i++;
	if (i == 0 || (line[i] != ' ' && line[i] != '\t' && line[i] != '\0'))
		return (84);
	while (line[i] == ' ' || line[i] == '\t')
		i++;
	if (depth < 2) {
		if (depth == 0)
			tetrimino->width = my_getnbr(line);
		else
			tetrimino->height = my_getnbr(line);
		return (parse_data(tetrimino, &line[i], depth + 1));
	} else if (line[i] != '\0')
		return (84);
	tetrimino->color = my_getnbr(line);
	return (tetrimino->color > 7);
}

static int	get_data(tetrimino_t *tetrimino, int fd)
{
	char *line = get_next_line(fd);

	if (!line || parse_data(tetrimino, line, 0) != 0)
		return (clear_buffer(fd));
	free(line);
	tetrimino->shape = malloc(sizeof(char *) * (tetrimino->height + 1));
	if (tetrimino->shape == NULL || get_shape(tetrimino, fd) == 84)
		return (clear_buffer(fd));
	return (0);
}

tetrimino_t	*create_tetrimino(char *filename)
{
	tetrimino_t *tetrimino = NULL;
	char dirpath[PATH_MAX] = "tetriminos/\0";
	int fd = open(my_strncat(dirpath, filename, PATH_MAX - 11), O_RDONLY);
	char *name = NULL;

	tetrimino = malloc(sizeof(tetrimino_t));
	name = malloc(sizeof(char) * (my_strlen(filename) - 9));
	if (name == NULL || tetrimino == NULL)
		return (NULL);
	my_strncpy(&name[0], filename, my_strlen(filename) - 10);
	tetrimino->name = name;
	if (fd != -1) {
		if (get_data(tetrimino, fd) == 84)
			tetrimino->width = 0;
		close(fd);
	}
	return (tetrimino);
}
