/*
** EPITECH PROJECT, 2018
** Tetris
** File description:
** create frame for the game
*/

#include <stdlib.h>

char	*create_extrem_line(int col, char *frame)
{
	int idx = 1;

	frame = malloc(sizeof(char) * ((col + 1) * 2 + 1));
	if (frame == NULL)
		return (NULL);
	frame[0] = '+';
	while (idx < ((col + 1) * 2 - 1)) {
		frame[idx] = '-';
		idx++;
	}
	frame[idx] = '+';
	frame[((col + 1) * 2)] = '\0';
	return (frame);
}

char	*create_mid_line(int col, char *frame)
{
	int idx = 1;

	frame = malloc(sizeof(char) * ((col + 1) * 2 + 1));
	if (frame == NULL)
		return (NULL);
	frame[0] = '|';
	while (idx < ((col + 1) * 2 - 1)) {
		frame[idx] = ' ';
		idx++;
	}
	frame[idx] = '|';
	frame[((col + 1) * 2)] = '\0';
	return (frame);
}

char	**get_game_frame(int row, int col)
{
	char **frame = malloc(sizeof(char*) * (row + 3));
	int idx = 1;

	if (frame == NULL)
		return (NULL);
	frame[0] = create_extrem_line(col, frame[0]);
	while (idx <= row) {
		frame[idx] = create_mid_line(col, frame[idx]);
		if (frame[idx] == NULL)
			return (NULL);
		idx++;
	}
	frame[idx] = create_extrem_line(col, frame[idx]);
	frame[(idx + 1)] = NULL;
	if (frame[0] == NULL || frame[idx] == NULL)
		return (NULL);
	return (frame);
}
