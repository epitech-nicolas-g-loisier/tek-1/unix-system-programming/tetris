/*
** EPITECH PROJECT, 2018
** tetris
** File description:
** Closes the ncurses win and frees the mem used
*/

#include <curses.h>
#include <stdlib.h>
#include "tetris.h"

static void	free_tetrimino(tetrimino_t *piece)
{
	for (int i = 0; i < piece->height; i++)
		free(piece->shape[i]);
	free(piece->name);
	free(piece->shape);
	free(piece);
}

static void	free_frame(char **frame)
{
	for (int i = 0; frame[i] != NULL; i++)
		free(frame[i]);
	free(frame);
}

void	end_win(tetrimino_t **pieces, char *map, char **frame)
{
	for (int i = 0; pieces[i] != NULL; i++)
		free_tetrimino(pieces[i]);
	free(pieces);
	free_frame(frame);
	free(map);
	endwin();
}
