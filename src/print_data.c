/*
** EPITECH PROJECT, 2018
** tetris
** File description:
** Prints the map data
*/

#include <curses.h>

void	print_tile(char tile)
{
	if (tile != ' ') {
		attron(COLOR_PAIR(tile - 6 * (tile / 7)));
		addch(' '|A_REVERSE);
		addch(' '|A_REVERSE);
		attron(COLOR_PAIR(1));
	}
}

void	print_data(char *map, int *map_size)
{
	for (int i = 0; i < map_size[0]; i++) {
		for (int j = 0; j < map_size[1]; j++) {
			move(1 + i, 34 + j * 2);
			print_tile(map[i * 3 + j]);
		}
	}
}
