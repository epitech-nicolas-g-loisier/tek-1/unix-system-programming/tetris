/*
** EPITECH PROJECT, 2018
** tetris
** File description:
** Prints the tetrimino's data
*/

#include <ncurses.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <dirent.h>
#include "tetris.h"
#include "my.h"

bool	extension_is_tetrimino(char *path)
{
	int len = my_strlen(path);

	if (len <= 10)
		return (false);
	if (my_strcmp(&path[len - 10], ".tetrimino"))
		return (false);
	return (true);
}

int	print_tetrimino(tetrimino_t *ent)
{
	my_putstr("Tetriminos :  Name ");
	my_putstr(ent->name);
	if (ent->width == 0 || ent->height == 0 || ent->shape == NULL) {
		my_putstr(" :  Error\n");
		return (84);
	}
	my_putstr(" :  Size ");
	my_put_nbr(ent->width);
	my_putchar('*');
	my_put_nbr(ent->height);
	my_putstr(" :  Color ");
	my_put_nbr(ent->color);
	my_putstr(" :\n");
	for (int i = 0; i < ent->height; i++) {
		my_putstr(ent->shape[i]);
		my_putchar('\n');
	}
	return (0);
}

int	sort_tetriminos(tetrimino_t **tetriminos, int size)
{
	tetrimino_t *temp;
	int i = 0;
	int change = 0;
	int diff = 0;

	while (change != 0 || i + 1 != size) {
		diff = str_acmp(tetriminos[i]->name, tetriminos[i + 1]->name);
		if (diff > 0) {
			temp = tetriminos[i];
			tetriminos[i] = tetriminos[i + 1];
			tetriminos[i + 1] = temp;
			change += 1;
		}
		if (i + 2 >= size && change != 0) {
			change = 0;
			i = 0;
		} else
			i++;
	}
	return (0);
}

int	get_tetriminos_count(void)
{
	DIR *fd = opendir("./tetriminos");
	struct dirent *file = NULL;
	int count = 0;

	if (fd == NULL)
		return (-1);
	file = readdir(fd);
	while (file) {
		count += extension_is_tetrimino(file->d_name);
		file = readdir(fd);
	}
	closedir(fd);
	return (count);
}

int	start_debug_mode(char controls[6][6], int options[4])
{
	tetrimino_t **tetriminos = NULL;
	int tetriminos_count = get_tetriminos_count();

	if (tetriminos_count == -1) {
		write(2, "tetris: tetriminos dir not found\n", 33);
		return (84);
	}
	print_config(controls, options);
	my_putstr("\nTetriminos :  ");
	my_put_nbr(tetriminos_count);
	my_putchar('\n');
	tetriminos = get_tetriminos(tetriminos_count);
	if (tetriminos_count && tetriminos) {
		sort_tetriminos(tetriminos, tetriminos_count);
		for (int i = 0; i < tetriminos_count; i++)
			print_tetrimino(tetriminos[i]);
	}
	my_putstr("Press any key to start Tetris\n");
	wait_input();
	return (tetris(controls, options, tetriminos));
}
