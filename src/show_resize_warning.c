/*
** EPITECH PROJECT, 2017
** show_resize_warning
** File description:
** Prints a warning in the middle of the screen
*/

#include <ncurses.h>
#include <unistd.h>
#include "my.h"

void	pause_game(char exit_key[6])
{
	char buffer[6] = "";

	raw();
	while (my_strcmp(buffer, exit_key) != 0) {
		erase();
		mvprintw(LINES / 2, COLS / 2 - 2, "Pause");
		refresh();
		read(0, buffer, 5);
	}
	noraw();
}

void	show_resize_warning(int *size)
{
	int status = 0;

	while (COLS <= size[1] * 2 + 34 || LINES <= size[0] + 2) {
		erase();
		mvprintw(LINES / 2, COLS / 2 - 11, "Resize to a bigger size");
		refresh();
		status = 1;
	}
	if (status)
		erase();
}
