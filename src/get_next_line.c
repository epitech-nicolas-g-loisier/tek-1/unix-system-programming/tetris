/*
** EPITECH PROJECT, 2018
** get_next_line
** File description:
** Returns the next line from the feed given as arg
*/

#include <unistd.h>
#include <stdlib.h>
#include "get_next_line.h"

char	*change_alloc(char *line, int counter)
{
	char *dest = malloc(sizeof(char) * (READ_SIZE + counter + 1));

	if (dest == NULL) {
		free(line);
		return (NULL);
	}
	if (line != NULL)
		dest[0] = line[0];
	while (counter > 0) {
		dest[counter] = line[counter];
		counter = counter - 1;
	}
	free(line);
	return (dest);
}

char	*copy_buffer(int pos, int *counter, char *buffer, char *line)
{
	if (READ_SIZE != 0 && *counter / READ_SIZE * READ_SIZE == *counter)
		line = change_alloc(line, *counter);
	if (line == NULL)
		return (NULL);
	line[*counter] = buffer[pos];
	*counter = *counter + 1;
	return (line);
}

char	*correct_return_value(char *line, int counter)
{
	if (line == NULL)
		line = malloc(sizeof(char) * 1);
	if (line == NULL)
		return (NULL);
	line[counter] = '\0';
	return (line);
}

char	*get_next_line(int fd)
{
	char *line = NULL;
	static char buffer[READ_SIZE + 1];
	static int pos = READ_SIZE;
	static int read_c = READ_SIZE + 1;
	int counter = 0;

	if (pos < 0)
		return (NULL);
	while (buffer[pos] != '\n' && pos <= read_c) {
		if (pos >= READ_SIZE) {
			for (int i = 0; i < READ_SIZE; i++)
				buffer[i] = '\0';
			read_c = read(fd, buffer, READ_SIZE);
			if (read_c < 1) {
				pos = READ_SIZE;
				read_c = pos + 1;
				return (NULL);
			}
			pos = 0;
		}
		if (buffer[pos] != '\n') {
			line = copy_buffer(pos++, &counter, buffer, line);
			if (line == NULL)
				return (NULL);
		}
	}
	pos = pos + 1;
	if (pos >= read_c) {
		pos = READ_SIZE;
		read_c = pos + 1;
	}
	return (correct_return_value(line, counter));
}
