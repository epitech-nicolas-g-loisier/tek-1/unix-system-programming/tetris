/*
** EPITECH PROJECT, 2018
** tetris
** File description:
** Creates the tetriminos list from file
*/

#include <dirent.h>
#include <stdlib.h>
#include "tetris.h"
#include "my.h"

static int	check_tetrimino_count(int tetrimino_count)
{
	DIR *fd = NULL;
	struct dirent *file = NULL;
	int count = 0;

	if (tetrimino_count > -1)
		return (tetrimino_count);
	fd = opendir("./tetriminos");
	if (fd == NULL)
		return (-1);
	file = readdir(fd);
	while (file) {
		count += extension_is_tetrimino(file->d_name);
		file = readdir(fd);
	}
	closedir(fd);
	return (count);
}

static tetrimino_t	**fill_list(DIR *fd, tetrimino_t **list)
{
	int i = 0;
	struct dirent *file = NULL;

	file = readdir(fd);
	while (file) {
		if (extension_is_tetrimino(file->d_name)) {
			list[i] = create_tetrimino(file->d_name);
			if (list[i] == NULL)
				return (NULL);
			i++;
		}
		file = readdir(fd);
	}
	return (list);
}

tetrimino_t	**get_tetriminos(int tetriminos_count)
{
	int t_count = check_tetrimino_count(tetriminos_count);
	DIR *fd = NULL;
	tetrimino_t **list = NULL;

	if (t_count == -1)
		return (NULL);
	fd = opendir("tetriminos");
	list = malloc(sizeof(tetrimino_t *) * (t_count + 1));
	if (fd == NULL || list == NULL)
		return (NULL);
	list[t_count] = NULL;
	fill_list(fd, list);
	closedir(fd);
	return (list);
}
