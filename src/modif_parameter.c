/*
** EPITECH PROJECT, 2018
** Tetris
** File description:
** modif default parameter of tetris
*/

#include <stdio.h>
#include <unistd.h>
#include "my.h"
#include "tetris.h"

int	modif_option(int option[4], char **argv, int *idx, int ret)
{
	int	new_opt = 0;
	static int size = 0;

	if (ret == 0) {
		option[0] = 0;
		return (0);
	} else if (argv[*idx][0] == '-' && argv[*idx][1] != '-')
		*idx += 1;
	new_opt = my_getnbr(&argv[*idx][6]);
	if (new_opt <= 0) {
		write(2, "error: Invalid option given\n", 28);
		return (84);
	} else if (size == 0 && ret == 2) {
		size = 1;
	} else
		if (size == 1 && ret == 2) {
			ret += 1;
			size = 0;
		}
	option[ret] = new_opt;
	return (0);
}

int	get_long_key(char *argv, char new_key[5])
{
	int idx = 0;
	int tmp = 0;

	while (argv[(tmp - 1)] != '=' && argv[tmp] != '\0') {
		tmp++;
	}
	if (argv[tmp] == '\0')
		return (84);
	else if (argv[tmp] == '{')
		tmp++;
	while (argv[tmp] != '}' && argv[tmp] != '\0' && idx < 5) {
		new_key[idx] = argv[tmp];
		tmp++;
		idx++;
	}
	if (idx == 5 && (argv[tmp] != '\0' || argv[tmp] != '}'))
		return (84);
	new_key[idx] = '\0';
	return (0);
}

int	get_new_key(char *argv, char new_key[5])
{
	int idx = 0;

	if (argv == NULL || (argv[0] != '-' && my_strlen(argv) > 4))
		return (84);
	else if (argv[0] != '-') {
		while (argv[idx] != '\0') {
			new_key[idx] = argv[idx];
			idx++;
		}
		new_key[idx] = '\0';
		return (0);
	}
	else {
		idx = get_long_key(argv, &new_key[0]);
	}
	return (idx);
}

int	modif_key(char key[6][6], char **argv, int *idx, int pos)
{
	char new_key[6] = "";
	int ret = 0;

	if (argv[*idx] == NULL)
		return (84);
	else if (argv[*idx][0] == '-' && argv[*idx][1] != '-')
		*idx += 1;
	ret = get_new_key(argv[*idx], &new_key[0]);
	if (ret == 84)
		return (84);
	else {
		my_strcpy(&key[pos][0], new_key);
	}
	return (0);
}
