/*
** EPITECH PROJECT, 2018
** Tetris
** File description:
** Print tetrimino
*/

#include <stdlib.h>
#include <ncurses.h>
#include "tetris.h"

int	print_square(int x, int y, int i, int o)
{
	attron(A_REVERSE);
	mvprintw(x + i, y + o, " ");
	mvprintw(x + i, y + o + 1, " ");
	attroff(A_REVERSE);
	return (0);
}
int	print_tetri_two(tetrimino_t *tetrimino, int x, int y)
{
	int color = tetrimino->color;
	int col = 0;
	int i = 0;

	if (tetrimino->width == 0 || tetrimino->height == 0)
		return (84);
	attron(COLOR_PAIR(color));
	for (int o = 0; tetrimino->shape[i][o] != '\0'; o++) {
		while (tetrimino->shape[i] != NULL) {
			if (tetrimino->shape[i][o] == '*') {
				print_square(x, y, o, col);
			}
			col += 2;
			i++;
		}
		i = 0;
		col = 0;
	}
	attroff(COLOR_PAIR(color));
	return (0);
}

int	print_tetri_one(tetrimino_t *tetrimino, int x, int y)
{
	int color = tetrimino->color;
	int col = 0;

	if (tetrimino->width == 0 || tetrimino->height == 0)
		return (84);
	attron(COLOR_PAIR(color));
	for (int i = 0; tetrimino->shape[i] != NULL; i++) {
		col = 0;
		for (int o = 0; tetrimino->shape[i][o] != '\0'; o++) {
			if (tetrimino->shape[i][o] == '*') {
				print_square(x, y, i, col);
			}
			col += 2;
		}
	}
	attroff(COLOR_PAIR(color));
	return (0);
}
