/*
** EPITECH PROJECT, 2018
** Tetris
** File description:
** Get all the tetris option
*/

#include <stdio.h>
#include <unistd.h>
#include <getopt.h>
#include "my.h"
#include "tetris.h"

int	check_option(char arg)
{
	char opt[11] = "hwLslrtdqpD";
	int idx = 0;

	if (arg == -1)
		return (-1);
	while (idx < 11) {
		if (opt[idx] == arg)
			break;
		idx++;
	}
	return (idx);
}

int	find_option(int argc, char **argv)
{
	struct option option[12] = {{"help", 0, NULL, 'h'},
					{"without-next", 0, NULL, 'w'},
					{"level", 1, NULL, 'L'},
					{"map-size", 1, NULL, 's'},
					{"key-left", 1, NULL, 'l'},
					{"key-right", 1, NULL, 'r'},
					{"key-turn", 1, NULL, 't'},
					{"key-drop", 1, NULL, 'd'},
					{"key-quit", 1, NULL, 'q'},
					{"key-pause", 1, NULL, 'p'},
					{"debug", 0, NULL, 'D'}};
	char arg = 0;
	int ret = 0;

	arg = getopt_long(argc, argv, "wLlrtdqpD", option, NULL);
	ret = check_option(arg);
	return (ret);
}

bool	cmp_arg(char *arg, int opt)
{
	char *option[12] = {"help", "without-next", "level", "map-size",
				"key-left", "key-right", "key-turn", "key-drop",
				"key-quit", "key-pause", "debug", NULL};
	int length = my_strlen(option[opt]);

	if (arg[1] != '-' || opt == 11)
		return (true);
	else if (my_strncmp(option[opt], &arg[2], length) == 0)
		return (true);
	else {
		write(2, "error: invalid option\n", 22);
		return (false);
	}
}

int	get_opt_loop(char **argv, int option[4], char key[6][6], int opt)
{
	static int idx = 1;
	int ret = 0;
	static int debug = 0;

	if (cmp_arg(argv[idx], opt) == false)
		return (84);
	if (opt == 0) {
		display_help(argv[0]);
		return (-1);
	} else if (opt > 0 && opt < 4)
		ret = modif_option(&option[0], argv, &idx, (opt - 1));
	else {
		if (opt > 3 && opt < 10)
			ret = modif_key(&key[0], argv, &idx, (opt - 4));
		else if (opt == 10)
			debug = 1;
		else
			return (84);
	}
	idx++;
	return ((ret + debug));
}

int	get_opt(int argc, char **argv, int option[4], char key[6][6])
{
	int debug = 0;
	int opt = 0;

	opt = find_option(argc, argv);
	while (opt != -1 && debug != -1) {
		debug = get_opt_loop(argv, &option[0], &key[0], opt);
		if (debug == 84)
			return (84);
		else if (debug == -1)
			return (-1);
		opt = find_option(argc, argv);
	}
	if (debug > 1) {
		write(2, "error: invalid option\n", 22);
		return (84);
	} else
		return (debug);
}
