/*
** EPITECH PROJECT, 2018
** Tetris
** File description:
** display data of game
*/

#include <stdlib.h>
#include <curses.h>
#include <time.h>
#include "tetris.h"

void	print_name(int x, int y, char *name)
{
	attron(A_UNDERLINE);
	mvprintw(x, y, "%s", name);
	attroff(A_UNDERLINE);
}

void	time_to_str(int start, int pres, char str[9])
{
	int sec = pres - start;
	int min = 0;
	int hour = 0;

	min = sec / 60;
	sec = sec % 60;
	hour = min / 60;
	min = min % 60;
	str[7] = sec % 10 + '0';
	str[6] = sec / 10 + '0';
	str[4] = min % 10 + '0';
	str[3] = min / 10 + '0';
	str[1] = hour % 10 + '0';
	str[0] = hour / 10 + '0';
}

void	print_time(void)
{
	static int start = 0;
	int pres = time(NULL);
	char str[9] = "00:00:00\0";

	if (start == 0)
		start = time(NULL);
	time_to_str(start, pres, &str[0]);
	print_name(10, 2, "Time :");
	mvprintw(10, 20, "%s", str);
}

int	print_info(char **frame)
{
	int idx = 0;

	init_pair(1, COLOR_RED, COLOR_BLACK);
	attron(COLOR_PAIR(1));
	attron(A_BOLD);
	while (frame[idx] != NULL) {
		mvprintw(idx, 0, "%s", frame[idx]);
		idx++;
	}
	attroff(COLOR_PAIR(1));
	print_time();
	return (0);
}

int	tetris_info(int score, int line, int level)
{
	static char **frame = NULL;
	static int high_score = 999999999;

	if (frame == NULL)
		frame = get_game_frame(11, 14);
	print_info(frame);
	print_name(2, 2, "High Score :");
	mvprintw(2, 19, "%9d", high_score);
	print_name(4, 2, "Score :");
	mvprintw(4, 19, "%9d", score);
	print_name(6, 2, "Line :");
	mvprintw(6, 19, "%9d", line);
	print_name(8, 2, "Level :");
	mvprintw(8, 19, "%9d", level);
	return (0);
}
