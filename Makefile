##
## EPITECH PROJECT, 2017
## Makefile
## File description:
## Compiles program
##

SRC	=	$(SRC_DIR)/debug_mode.c	\
		$(SRC_DIR)/wait_input.c	\
		$(SRC_DIR)/print_config.c	\
		$(SRC_DIR)/get_tetriminos.c	\
		$(SRC_DIR)/get_next_line.c	\
		$(SRC_DIR)/create_tetrimino.c	\
		$(SRC_DIR)/get_opt.c	\
		$(SRC_DIR)/main.c	\
		$(SRC_DIR)/modif_parameter.c	\
		$(SRC_DIR)/tetris.c	\
		$(SRC_DIR)/show_resize_warning.c	\
		$(SRC_DIR)/get_game_frame.c	\
		$(SRC_DIR)/tetris_info.c	\
		$(SRC_DIR)/print_data.c	\
		$(SRC_DIR)/create_map.c	\
		$(SRC_DIR)/get_shape.c	\
		$(SRC_DIR)/end_win.c	\
		$(SRC_DIR)/print_tetrimino.c

SRC_DIR	=	$(realpath ./src)

DEST_A	=	$(realpath ./lib/my)

NAME	=	tetris

OBJ	=	$(SRC:.c=.o)

CC	=	gcc

CFLAGS	=	-Wall -Wextra	\
		-Iinclude

LFLAGS	=	-L$(DEST_A) -lmy -lncurses

$(NAME):	$(OBJ)
	make --no-print-directory -C $(DEST_A)
	gcc -o $(NAME) $(OBJ) $(LFLAGS)

all:	$(NAME)

clean:
	rm -f $(OBJ)
	make --no-print-directory -C $(DEST_A) clean

fclean:
	rm -f $(NAME) $(OBJ)
	make --no-print-directory -C $(DEST_A) fclean

re:	fclean all

.PHONY: all clean fclean re
