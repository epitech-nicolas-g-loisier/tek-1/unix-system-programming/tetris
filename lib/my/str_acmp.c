/*
** EPITECH PROJECT, 2017
** str_alpha_cmp
** File description:
** compare two strings in alphabetic order
*/

#include <stdlib.h>
#include <stdio.h>
#include "my.h"

int	str_acmp(char const *s1, char const *s2)
{
	int i = 0;
	int ret = 0;
	char *tmps1;
	char *tmps2;

	tmps1 = my_strlowcase(s1);
	tmps2 = my_strlowcase(s2);
	if (tmps1 == NULL || tmps2 == NULL)
		return (0);
	while (tmps1[i] != '\0' && tmps2[i] != '\0'){
		if (tmps1[i] == tmps2[i]){
			i++;
		} else if (tmps1[i] != tmps2[i]){
			ret = tmps1[i] - tmps2[i];
			break;
		}
	}
	free(tmps1);
	free(tmps2);
	return (ret);
}
