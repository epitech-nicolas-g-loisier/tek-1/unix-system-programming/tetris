/*
** EPITECH PROJECT, 2017
** my_strlen
** File description:
** Counts length of string
*/

#include <stdlib.h>

int	my_strlen(char const *str)
{
	int size = 0;

	if (str == NULL)
		return (-1);
	while (str[size] != '\0')
		size++;
	return (size);
}
