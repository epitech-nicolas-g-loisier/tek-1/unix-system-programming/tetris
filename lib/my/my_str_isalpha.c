/*
** EPITECH PROJECT, 2017
** my_str_isalpha
** File description:
** Searches if string is alpha only
*/

int	my_str_isalpha(char const *str)
{
	int argc = 0;

	while (str[argc] != '\0') {
		if (str[argc] < 65 || str[argc] > 122)
			return (0);
		if (str[argc] < 97 && str[argc] > 90)
			return (0);
		argc = argc + 1;
	}
	return (1);
}
