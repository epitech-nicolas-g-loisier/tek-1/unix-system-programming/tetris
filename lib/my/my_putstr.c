/*
** EPITECH PROJECT, 2017
** my_putstr
** File description:
** Print a string
*/

#include <unistd.h>

void	my_putstr(char const *str)
{
	int size = 0;

	while (str[size] != '\0')
		size++;
	write(1, str, size);
}
