/*
** EPITECH PROJECT, 2018
** Tetris
** File description:
** tetris.h
*/

#include <stdbool.h>

#ifndef TETRIMINO_T_
#define TETRIMINO_T_

typedef struct tetrimino
{
	char *name;
	char **shape;
	int width;
	int height;
	int color;
} tetrimino_t;

#endif /* TETRIMINO_T_ */

#ifndef TETRIS_H_
#define TETRIS_H_

void	end_win(tetrimino_t **, char *, char **);

int	get_shape(tetrimino_t *, int);

void	show_resize_warning(int *);

int	tetris(char[6][6], int[4], tetrimino_t **);

void	print_config(char[6][6], int *);

void	wait_input(void);

tetrimino_t	**get_tetriminos(int);

bool	extension_is_tetrimino(char *);

tetrimino_t	*create_tetrimino(char *);

void	display_help(char*);

int	modif_option(int*, char**, int*, int);

int	modif_key(char[6][6], char**, int*, int);

int	get_opt(int, char**, int*, char[6][6]);

int	start_debug_mode(char[6][6], int*);

char	**get_game_frame(int, int);

int	tetris_info(int, int, int);

char	*create_map(int *);

void	print_data(char *, int *);

int	print_tetri_one(tetrimino_t*, int, int);

int	print_tetri_two(tetrimino_t*, int, int);

#endif /* TETRIS_H_ */
