## B2 - Unix System Programming

## B-PSU-tetris

# tetris

```
binary name : tetris
language : C
compilation : via Makefile, including re, clean and fclean rules
```

- Your repository must contain the totality of your source files, but no useless files (binary, temp files, obj files,...).
- All the bonus files (including a potential specific Makefile) should be in a directory named _bonus_.
- Error messages have to be written on the error output, and the program should then exit with the 84 error code (0 if there is no error).


Authorized functions : rand , srand , getopt , getopt_long , clock , and all functions used for PSU projects until this point

The goal of this project is to recreate the Tetris game in a UNIX terminal, with the Gameboy version rules.\
You have to use ncurses.\
In the folder of your binary, there must be a _tetriminos_ directory, which contains files that describe the game pieces.

```shell
∼/B-PSU-200> ls ./tetriminos/
bar.tetrimino           square.tetrimino    5.tetrimino     7.tetrimino
inverted-L.tetrimino    4.tetrimino         6.tetrimino
```
These files are composed in the following way:

- on the first line, the size and color of the piece in this format: _width height color_code\n_ (the number of the color corresponds to the ncurses capacity’s color numbers),
- on the _h_ following lines (where _h_ is the height of the tetrimino), the piece’s shape composed with asterisks (*) and spaces (‘ ’).

For instance, these pieces correspond to the opposite files:
```shell
∼/B-PSU-200> cat -e bar.tetrimino
1 4 2$
* $
* $
*$
* $
∼/B-PSU-200> cat -e 6.tetrimino
2 3 6$
**$
*$
**$
```

The pieces (randomly chosen) fall from the top of the map and pile up on the bottom. Each time a line is completed, it disappears, leaving all of the pieces above it to fall.\
The level increases by 1 for every 10 deleted lines. The falling speed increases proportionally to the level.\
When it is no longer possible for pieces to fall from the top of the map, the player loses.

When the game begins, the terminal must delete all content. Then, it must show (at least):
- the main map,
- a preview of the next tetrimino to fall,
- current score, high score, current number of completed lines, level, timer.

If the terminal is too small to host the map, the game doesn’t start; an error message is printed, asking the user to enlarge his/her terminal.

For the gameplay, refer to the following usage:

```shell
∼/B-PSU-200> ./tetris --help | cat -e
Usage: ./tetris [options]$
Options:$
--help Display this help$
-L --level={num} Start Tetris at level num (def: 1)$
-l --key-left={K} Move the tetrimino LEFT using the K key (def: left arrow)$
-r --key-right={K} Move the tetrimino RIGHT using the K key (def: right arrow)$
-t --key-turn={K} TURN the tetrimino clockwise 90d using the K key (def: top arrow)$
-d --key-drop={K} DROP the tetrimino using the K key (def: down arrow)$
-q --key-quit={K} QUIT the game using the K key (def: ‘q’ key)$
-p --key-pause={K} PAUSE/RESTART the game using the K key (def: space bar)$
--map-size={row,col} Set the numbers of rows and columns of the map (def: 20,10)$
-w --without-next Hide next tetrimino (def: false)$
-D --debug Debug mode (def: false)$
```

Feel free to choose the graphic style you want

## Debug mode


Your project will be evaluated solely through the debug mode, implement it scrupulously before starting to develop the game proper

When in debug mode, display the following information before starting the game (until the user presses a key):

```shell
∼/B-PSU-200> ./tetris -d ‘x’ -D --key-turn=‘ ’ -p ‘p’
*** DEBUG MODE ***
Key Left : ˆEOD
Key Right : ˆEOC
Key Turn : (space)
Key Drop : x
Key Quit : q
Key Pause : p
Next : Yes
Level : 1
Size : 20*10
Tetriminos : 7
Tetriminos : Name 4 : Error
Tetriminos : Name 5 : Size 1*1 : Color 4 :
*
Tetriminos : Name 6 : Size 2*3 : Color 6 :
*
**
 *
Tetriminos : Name 7 : Size 5*4 : Color 3 :
 * *
* * *
 * *
  *
Tetriminos : Name bar : Size 1*4 : Color 2 :
*
*
*
*
Tetriminos : Name inverted-L : Size 2*3 : Color 5 :
 *
 *
**
Tetriminos : Name square : Size 2*2 : Color 1 :
**
**
Press any key to start Tetris
```

Tertiminos are to be displayed by alphabetical order.

## Bonus

Many nice bonus points are possible.\
Unleash your imagination, and have fun:

- save the round to play again later,
- save High Scores with player names (hall of fame),
- High Score board,
- miscellaneous animations,
- music and sound effects,
- multi-player game (cf tetriNET for instance).